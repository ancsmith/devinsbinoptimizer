#!/usr/bi:n/env python

# Set up ROOT and RootCore:
from ROOT import *
from math import *

import time
import sys
import numpy as np
from root_numpy import hist2array,array2hist
#import matplotlib
from matplotlib import pyplot as plt
from matplotlib.pyplot import cm
#import scipy#.optimize import minimize

cat = 'nlp_2ph'
tmin = 0.0   #-17.5
tmax = 12.0  #17.5
BR = 0.05
ROOTFile = 'files/elena4/combined.root'
#ROOTFile = 'test.root'
dataHist = 'TIMING_POINTING_2GeV__SR_{0}_data'.format(cat)
signalHist = 'TIMING_POINTING_2GeV__SR_{0}_60_0_2'.format(cat)
ntMax = 3
nzMax = 3

fText = open('results/bins/results_{0}_t{1}_z{2}.txt'.format(signalHist,ntMax,nzMax),'w')

class Unbuffered:

 def __init__(self, stream):
   self.stream = stream
 def write(self, data):
   self.stream.write(data)
   self.stream.flush()
   fText.write(data)    # Write the data of stdout here to a text file as well

def formatData(ROOTFile,dataHist,signalHist,BR,tmin,tmax):

  f = TFile(ROOTFile)

  dataInput     = f.Get(dataHist)
  signalInput   = f.Get(signalHist)

  dataInput.RebinX(2)
  signalInput.RebinX(2)

  # Scale signal by desired BR
  signalInput.Scale(BR)

  data          = hist2array(dataInput)
  signal        = hist2array(signalInput)

  # Add all negative z to positive side to get |z|
  for i in range(0,2000):
    for j in range(200,400):
      signal[i,j] += signal[i,399 - j]
      if i >= 1000: 
        continue
      data[i,j] += data[i,399 - j]
      data[1999 - i,j] = data[i,j]

  # Remove obsolete negative pointing and restrict t to specified range
  data          = data[int(10*(tmin + 100)):int(10*(tmax + 100)),200:400]
  signal        = signal[int(10*(tmin + 100)):int(10*(tmax + 100)),200:400]

  #print "Entries: " + str(np.sum(data,axis=1))
  #print str(np.sum(data[0,:])) + " " + str(np.sum(data[1,:]))

  f.Close()

  return [data,signal]

def savePlots(data,signal,tBins,zBins):

  tBins = [int(10. * (x - tmin)) for x in tBins]
  zBins = [int(0.1 * x) for x in zBins]

  # Number of events 2D plots
  nEventsData = TH2F('nEventsData','',len(tBins) - 1,-0.5,len(tBins) - 1.5,len(zBins) - 1,-0.5,len(zBins) - 1.5)
  nEventsSig  = TH2F('nEventsSig','',len(tBins) - 1,-0.5,len(tBins) - 1.5,len(zBins) - 1,-0.5,len(zBins) - 1.5)

  for i in range(0,len(tBins) - 1):
    for j in range(0,len(zBins) - 1):
      nEventsSig .SetBinContent(i + 1,j + 1,signal[tBins[i]:tBins[i + 1],zBins[j]:zBins[j + 1]].sum())
      nEventsData.SetBinContent(i + 1,j + 1,data[tBins[i]:tBins[i + 1],zBins[j]:zBins[j + 1]].sum())

  nEventsData.SetEntries(data.sum())
  nEventsSig .SetEntries(signal.sum())

  c1 = TCanvas('c1','',200,10,700,500)
  gStyle.SetOptStat(0)
  nEventsData.Draw('textcolz')
  c1.SaveAs('results/plots/nEventsData_{0}_t{1}of{2}_z{3}of{4}.png'.format(signalHist,len(tBins) - 1,ntMax,len(zBins) -1,nzMax))
  nEventsSig.Draw('textcolz')
  c1.SaveAs('results/plots/nEventsSig_{0}_t{1}of{2}_z{3}of{4}.png'.format(signalHist,len(tBins) - 1,ntMax,len(zBins) -1,nzMax))

  # Inclusive t and z plots
  ntBinsInput = int(350. * (tmax - tmin) / 35.)
  nzBinsInput = 200

  #histData = TH2F('histData','SR t < 0 Sym.',len(tBins) - 1,tBins[0],tBins[-1],len(zBins) - 1,zBins[0],zBins[-1])
  #histSignal = TH2F('histSignal','Signal',len(tBins) - 1,tBins[0],tBins[-1],len(zBins) - 1,zBins[0],zBins[-1])
  histData = TH2F('histData','SR t < 0 Sym.',ntBinsInput,tmin,tmax,200,0,2000)
  histSignal = TH2F('histSignal','Signal',ntBinsInput,tmin,tmax,200,0,2000)
  #c1.SetLogy()
  histDataTime = array2hist(data,histData).ProjectionX()
  histDataPoint = array2hist(data,histData).ProjectionY()
  histSignalTime = array2hist(signal,histSignal).ProjectionX()
  histSignalPoint = array2hist(signal,histSignal).ProjectionY()
  histSignalTime.SetLineColor(kRed)
  histSignalPoint.SetLineColor(kRed)
  # t plots
  hStack = THStack('','')
  hStack.Add(histDataTime)
  hStack.Add(histSignalTime)
  hStack.Draw('nostack')
  hStack.GetXaxis().SetTitle('t [ns]')
  hStack.GetYaxis().SetTitle('Events')
  gPad.BuildLegend(0.75,0.75,0.95,0.95,'Inclusive')
  c1.Update()
  l = TLine(0,0,0,0)
  l.SetLineStyle(9)
  for i in range(0,len(tBins)):
    l.DrawLine(0.1 * tBins[i] + tmin,gPad.GetUymin(),0.1 * tBins[i] + tmin,gPad.GetUymax())
    l.Draw('same')
  c1.SetLogy()
  c1.SaveAs('results/plots/dataMCTime_{0}_t{1}of{2}_z{3}of{4}.png'.format(signalHist,len(tBins) - 1,ntMax,len(zBins) -1,nzMax))
  # z plots
  hStack = THStack('','')
  hStack.Add(histDataPoint)
  hStack.Add(histSignalPoint)
  hStack.Draw('nostack')
  hStack.GetXaxis().SetTitle('|z| [mm]')
  hStack.GetYaxis().SetTitle('Events')
  gPad.BuildLegend(0.75,0.75,0.95,0.95,'Inclusive')
  c1.Update()
  l = TLine(0,0,0,0)
  l.SetLineStyle(9)
  for i in range(0,len(zBins)):
    l.DrawLine(10. * zBins[i],gPad.GetUymin(),10. * zBins[i],gPad.GetUymax())
    l.Draw('same')
  c1.SetLogy()
  c1.SaveAs('results/plots/dataMCPoint_{0}_t{1}of{2}_z{3}of{4}.png'.format(signalHist,len(tBins) - 1,ntMax,len(zBins) -1,nzMax))


def significance(data,signal,tBins,zBins):

  sig = 0.0
  for i in range(0,len(tBins) - 1):
    for j in range(0,len(zBins) - 1):
      Sij = signal[tBins[i]:tBins[i + 1],zBins[j]:zBins[j + 1]].sum()
      Bij = data[tBins[i]:tBins[i + 1],zBins[j]:zBins[j + 1]].sum()
      if (Bij == 0):
        return -1
        #continue;
      elif (Sij != 0):
        #sig = hypot(sig,sqrt(2 * ((Sij + Bij) * log(1 + Sij / Bij) - Sij)))
        #sig = hypot(sig,1 / sqrt(1 / (2 * ((Sij + Bij) * log(1 + Sij / Bij) - Sij)) + Bij / Sij**2))
        #sig =  hypot(sig,1.0 / sqrt(-1.0 / (2.0 * (Bij * log(1.0 + Sij / Bij) - Sij)) + 1.0 / Sij))
        sig = hypot(sig,1.0 / sqrt(-1.0 / (2.0 * (Bij * log(1.0 + Sij / Bij) - Sij)) + Bij / Sij**2 + 1.0 / Sij))
      #else:
      #  print "WARNING: Empty signal and/or background bin" + " " + str(i) + " " + str(j)

  return sig

def optimize(data,signal,tBins,zBins,ntIter,nzIter):

  doRound = True

  #tBins = [0,ntBinsInput]
  #zBins = [0,200]

  # Set custom bins
  #tBins = [-17.5,-1.5,-1,0,1,1.5,2.5,4.5,6.5,9.5,12.5,17.5]
  #zBins = [0,50,100,150,200,250,500,2000]
  #tBins = [10. * (x - tmin) for x in tBins]
  #zBins = [0.1 * x for x in zBins]

  tmin = tBins[0]
  tmax = tBins[-1]
  ntBinsInput = int(350. * (tmax - tmin) / 35.)

  # Convert real t and z values to bin indices
  tBins = [int(10. * (x - tmin)) for x in tBins]
  zBins = [int(0.1 * x) for x in zBins]

  # Simultaneously optimize min(ntIter,nzIter) times
  maxSig = significance(data,signal,tBins,zBins)
  tEdges = [-1] * ntIter
  zEdges = [-1] * nzIter
  nIter = min(ntIter,nzIter)
  for n in range(0,nIter):
    for i in range(1,ntBinsInput + 1):
      # Exclude t bins below 1.0
      if (0.1 * i + tmin) <= 1.0:
        continue
      if doRound:
        # Only pick bins at multiples of 0.5 FIXME
        if (0.1 * i + tmin) % 0.5 != 0:
          continue
      for j in range(1,201):
        if doRound:
          # Only pick bins at multiples of 50 FIXME
          if (10 * j) % 50 != 0:
            continue 
        tBins.append(i)
        tBins.sort()
        zBins.append(j)
        zBins.sort()
        tempSig = significance(data,signal,tBins,zBins)
        if (tempSig > maxSig and not np.isclose(tempSig,maxSig)):
          maxSig = tempSig
          tEdges[n] = i
          zEdges[n] = j
        tBins.remove(i)
        zBins.remove(j)
    tBins.append(tEdges[n])
    tBins.sort()
    zBins.append(zEdges[n])
    zBins.sort()

  tBins.sort()
  zBins.sort()

  # Perform additional 1D optimizations as required by ntIter and nzIter
  maxSig = significance(data,signal,tBins,zBins)
  nIter = abs(ntIter - nzIter)
  for n in range(0,nIter):
    if ntIter > nzIter:
      for i in range(1,ntBinsInput + 1):
        # Exclude t bins below 1.0
        if (0.1 * i + tmin) < 1.0:
          continue
        if doRound:
          # Only pick bins at multiples of 0.5 FIXME
          if (0.1 * i + tmin) % 0.5 != 0:
            continue
        tBins.append(i)
        tBins.sort()
        tempSig = significance(data,signal,tBins,zBins)
        if (tempSig > maxSig and not np.isclose(tempSig,maxSig)):
          maxSig = tempSig
          tEdges[n] = i
        tBins.remove(i)
      if (tEdges[n] != -1):
        tBins.append(tEdges[n])
        tBins.sort()
      else:
        maxSig = -1
    else:
      for j in range(1,201):
        if doRound:
          # Only pick bins at multiples of 50 FIXME
          if (10 * j) % 50 != 0:
            continue
        zBins.append(j)
        zBins.sort()
        tempSig = significance(data,signal,tBins,zBins)
        if (tempSig > maxSig and not np.isclose(tempSig,maxSig)):
          maxSig = tempSig
          zEdges[n] = j
        zBins.remove(j)
      if (zEdges[n] != -1):
        zBins.append(zEdges[n])
        zBins.sort()
      else:
        maxSig = -1

  tBins.sort()
  zBins.sort()

  # Print out number of background events in each bin
  '''
  for i in range(0,len(tBins) - 1):
    for j in range(0,len(zBins) - 1):
      Bij = data[tBins[i]:tBins[i + 1],zBins[j]:zBins[j + 1]].sum()
      print "B{0}{1} = {2}".format(i,j,Bij)
  '''

  #Convert indices to real values
  tBins = [0.1 * x + tmin for x in tBins]
  zBins = [10 * x for x in zBins]

  return [tBins,zBins,maxSig]

# ------------------------------------------------------------------------------
def main():

  startTime = time.time()

  sys.stdout = Unbuffered(sys.stdout)

  [data,signal] = formatData(ROOTFile,dataHist,signalHist,BR,tmin,tmax)

  print "t = [" + str(tmin) + "," + str(tmax) + "]\tBR = " + str(BR) + "\tCat = " + cat

  #ntIter = 2            # Make ntIter + 1 bins (if optimal)
  #nzIter = 2            # Make nzIter + 1 bins (if optimal)

  gROOT.SetBatch(kTRUE)

  #gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )

  ntBinsInput = int(round(350. * (tmax - tmin) / 35.))
  nzBinsInput = 200

  # Run one optimization for multiple signals/channels
  '''
  dataHistTemp = dataHist #'ph_zdca_timing_SRtl0_cat_0_finebins'
  #signalPoint = ['60_0_2','30_25_2','30_0_2','40_35_2','50_40_2','60_50_2','60_55_2','60_0_10','50_40_10']
  #signalPoint = ['60_0_2','30_25_2','30_0_2','40_35_2','50_40_2','60_50_2','60_55_2']
  #signalPoint = ['40_10_10','40_0_10','60_50_10','50_30_10','50_20_10','50_10_10','50_0_10','60_40_10','60_30_10','60_20_10','60_10_10','60_0_10'] 
  #signalPoint = ['30_25_2','40_35_10','40_35_2','50_45_2','30_25_10','30_20_2','50_45_10','30_10_2','60_55_2','30_0_2','40_30_2','60_55_10','50_40_2','30_20_10','40_20_2','40_10_2','40_0_2','60_50_2','40_30_10','50_30_2','50_20_2','30_10_10','50_0_2','30_0_10','50_10_2','60_40_2','60_30_2','50_40_10','60_10_2','60_20_2','60_0_2','40_20_10']
  #signalPoint = ['30_25_2','40_35_10','40_35_2','50_45_2','30_25_10','30_20_2','50_45_10','30_10_2','60_55_2','30_0_2','40_30_2','60_55_10','50_40_2','30_20_10','40_20_2','40_10_2','40_0_2','60_50_2','40_30_10','50_30_2','50_20_2','30_10_10','50_0_2','30_0_10','50_10_2','60_40_2','60_30_2','50_40_10','60_10_2','60_20_2','60_0_2','40_20_10','40_10_10','40_0_10','60_50_10','50_30_10','50_20_10','50_10_10','50_0_10','60_40_10','60_30_10','60_20_10','60_10_10','60_0_10']
  #signalPoint = ['30_25_10','30_20_2','60_55_2','60_55_10','30_20_10','40_20_2','50_0_2','60_30_2','60_40_10']
  signalPoint = ['30_25_10']

  tBins = [tmin,tmax]
  zBins = [0,2000]
  ntIter = ntMax - 1
  nzIter = nzMax - 1

  print "# of t bins = " + str(ntIter + 1) + "\t # of z bins = " + str(nzIter + 1)

  tEdgesHist = TH2F('tEdgesHist',';Sample No.;t Edge No.',len(signalPoint),-0.5,len(signalPoint) - 0.5,ntIter + 5,-0.5,ntIter - 0.5 + 5)
  zEdgesHist = TH2F('zEdgesHist',';Sample No.;z Edge No.',len(signalPoint),-0.5,len(signalPoint) - 0.5,nzIter,-0.5,nzIter - 0.5)
  for i in range(0,len(signalPoint)):
    signalHistTemp = ' ph_zdca_timing_signal_finebins_{0}_SR_cat_{1}'.format(signalPoint[i],cat)
    print 'Signal file: {0}'.format(signalHistTemp)
    [dataTemp,signalTemp] = formatData(ROOTFile,dataHistTemp,signalHistTemp,BR,tmin,tmax)

    tBins = [tmin,0.2,0.4,0.6,0.8,1.0,1.2,1.3,tmax]
    zBins = [0,60,180,2000]

    # 2 separate 1D optimizations
    #tBins = [tmin,tmax]
    #[tBins,zBins,maxSig] = optimize(dataTemp,signalTemp,tBins,zBins,0,nzIter)
    #tBins = [tmin,0.2,0.4,0.6,0.8,1.0,tmax]
    #[tBins,zBins,maxSig] = optimize(dataTemp,signalTemp,tBins,zBins,ntIter,0)

    # Simultaneous optimization
    [tBins,zBins,maxSig] = optimize(dataTemp,signalTemp,tBins,zBins,ntIter,nzIter)

    print "Time Binning:"
    print tBins
    print "Pointing Binning: "
    print zBins
    print "Significance: " + str(maxSig) + "\n"

    for nt in range(1,len(tBins) - 1):
      tEdgesHist.Fill(i,nt - 1,tBins[nt])
    for nz in range(1,len(zBins) - 1):
      zEdgesHist.Fill(i,nz - 1,zBins[nz])

    savePlots(dataTemp,signalTemp,tBins,zBins)

  # Turn on/off plots
  makePlots = False
  if makePlots:

    hStack = THStack('','')
    #hTemp = TH1F('hTemp',';t Bin Edge [mm]',ntBinsInput,tmin,tmax)
    col = [kBlack,kRed,kBlue,kGreen+1,kOrange,kCyan+1,kMagenta,kGray,kYellow,kRed+2,kBlue-9,kRed-9,kGreen-9,kGreen+3,kYellow+3]
    for j in range(0,tEdgesHist.GetYaxis().GetNbins()):
      hTemp = TH1F('','Edge {0}'.format(j + 1),ntBinsInput,tmin - 0.05,tmax - 0.05)
      hTemp.SetLineColor(col[j])
      #hTemp.Reset()
      #hTemp.SetName('Edge {0}'.format(j))
      for i in range(0,tEdgesHist.GetXaxis().GetNbins()):
        edgeTemp = tEdgesHist.GetBinContent(i + 1,j + 1)
        if (not np.isclose(edgeTemp,0.2) and not np.isclose(edgeTemp,0.4) and not np.isclose(edgeTemp,0.6) and not np.isclose(edgeTemp,0.8) and not np.isclose(edgeTemp,1.0)):
          hTemp.Fill(edgeTemp)
      hStack.Add(hTemp)
      del(hTemp)

    hStack.Draw()
    hStack.GetXaxis().SetTitle('t [ns]')
    hStack.GetYaxis().SetTitle('No. of Edges')
    gPad.BuildLegend(0.75,0.75,0.95,0.95,'')
    c1.Update()
    c1.SaveAs('results/binEdgesBySample/tBinEdges_{0}_t{1}of{2}_z{3}of{4}.png'.format(signalHist,len(tBins) - 1,ntMax,len(zBins) - 1,nzMax))



    tEdgesArray = hist2array(tEdgesHist)
    edgeIndex = np.linspace(0,np.shape(tEdgesArray)[1] - 1,np.shape(tEdgesArray)[1])
    colorMap = cm.rainbow(np.linspace(0,1,np.shape(tEdgesArray)[0]))
    for i in range(np.shape(tEdgesArray)[0]):
      edgesTemp = tEdgesArray[i,:]
      plt.plot(edgeIndex,edgesTemp,label=signalPoint[i],c=colorMap[i])

    plt.legend(loc='upper left')
    plt.xlabel('t Edge Index')
    plt.ylabel('t [ns]')
    plt.savefig('results/binEdgesBySample/tVsEdgeIndex_t{0}_z{1}.png'.format(len(tBins) - 1,len(zBins) - 1))


    print tEdgesArray[:,5:]
    #print np.median(tEdgesArray[:,5:])
    #print (np.sum(tEdgesArray[:,5:]) - 3.8 * np.shape(tEdgesArray)[0]) / (tEdgesArray[:,5:].size - tEdgesArray)[0])


    hStack = THStack('','')
    for i in range(0,tEdgesHist.GetXaxis().GetNbins()):
      hTemp = TH1F('','{0}'.format(signalPoint[i]),ntBinsInput,tmin - 0.05,tmax - 0.05)
      hTemp.SetLineColor(col[i])
      for j in range(0,tEdgesHist.GetYaxis().GetNbins()):
        edgeTemp = tEdgesHist.GetBinContent(i + 1,j + 1)
        if (not np.isclose(edgeTemp,0.2) and not np.isclose(edgeTemp,0.4) and not np.isclose(edgeTemp,0.6) and not np.isclose(edgeTemp,0.8) and not np.isclose(edgeTemp,1.0)):
          hTemp.Fill(tEdgesHist.GetBinContent(i + 1,j + 1))
      hStack.Add(hTemp)
      del(hTemp)

    hStack.Draw()
    hStack.GetXaxis().SetTitle('t [ns]')
    hStack.GetYaxis().SetTitle('No. of Edges')
    gPad.BuildLegend(0.75,0.75,0.95,0.95,'')
    c1.Update()
    c1.SaveAs('results/binEdgesBySample/tBinEdgesSample_{0}_t{1}of{2}_z{3}of{4}.png'.format(signalHist,len(tBins) - 1,ntMax,len(zBins) -1,nzMax))



    fSave = TFile('results/binEdgesBySample/binEdges_{0}_t{1}_z{1}.root'.format(signalHist,ntMax,nzMax),'RECREATE')
    tEdgesHist.Write()
    zEdgesHist.Write()
    fSave.Close()
  '''


  # Examine average signal
  #'''
  dataHistTemp = dataHist #'ph_zdca_timing_SRtl0_cat_0_finebins'
  # High t
  #signalPoint = ['40_10_10','40_0_10','60_50_10','50_30_10','50_20_10','50_10_10','50_0_10','60_40_10','60_30_10','60_20_10','60_10_10','60_0_10']
  # Low t
  #signalPoint = ['30_25_2','40_35_10','40_35_2','50_45_2','30_25_10','30_20_2','50_45_10','30_10_2','60_55_2','30_0_2','40_30_2','60_55_10','50_40_2','30_20_10','40_20_2','40_10_2','40_0_2','60_50_2','40_30_10','50_30_2','50_20_2','30_10_10','50_0_2','30_0_10','50_10_2','60_40_2','60_30_2','50_40_10','60_10_2','60_20_2','60_0_2','40_20_10']
  # All
  signalPoint = ['30_25_2','40_35_10','40_35_2','50_45_2','30_25_10','30_20_2','50_45_10','30_10_2','60_55_2','30_0_2','40_30_2','60_55_10','50_40_2','30_20_10','40_20_2','40_10_2','40_0_2','60_50_2','40_30_10','50_30_2','50_20_2','30_10_10','50_0_2','30_0_10','50_10_2','60_40_2','60_30_2','50_40_10','60_10_2','60_20_2','60_0_2','40_20_10','40_10_10','40_0_10','60_50_10','50_30_10','50_20_10','50_10_10','50_0_10','60_40_10','60_30_10','60_20_10','60_10_10','60_0_10']
  # 10 ns
  #signalPoint = ['40_10_10','40_0_10','60_50_10','50_30_10','50_20_10','50_10_10','50_0_10','60_40_10','60_30_10','60_20_10','60_10_10','60_0_10','40_35_10','30_25_10','50_45_10','60_55_10','30_20_10','40_30_10','30_10_10','30_0_10','50_40_10','40_20_10']
  # 2 ns
  #signalPoint = ['30_25_2','40_35_2','50_45_2','30_20_2','30_10_2','60_55_2','30_0_2','40_30_2','50_40_2','40_20_2','40_10_2','40_0_2','60_50_2','50_30_2','50_20_2','50_0_2','50_10_2','60_40_2','60_30_2','60_10_2','60_20_2','60_0_2']
  #signalPoint = ['30_25_2']

  #eCuts = ["2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"]
  #eCuts = ["5","6","7","8","9","10","11","12","13","14","15","16","17"]
  eCuts = ["2","4"]

  catList = ['nlp_1ph','nlp_2ph']
  #catList = ['nlp_2ph']

  sigMat = np.zeros((len(catList),ntMax,nzMax,len(eCuts)))

  for nT in range(0,ntMax):
    for nZ in range(0,nzMax):
      print '[' + str(nT) + ',' + str(nZ) + '] ',
      i = 0
      j = 0
      for c in catList:
        for eCut in eCuts:
          #print "************************************"
          #print "E cut: " + str(eCut) + " GeV"
          #print "************************************"
          signalAvg = np.zeros((ntBinsInput,nzBinsInput)) 
          for s in signalPoint:
            dataHistTemp = 'TIMING_POINTING_{1}GeV__SR_{0}_data'.format(c,eCut)
            signalHistTemp = 'TIMING_POINTING_{2}GeV__SR_{0}_{1}'.format(c,s,eCut)
            [dataTemp,signalTemp] = formatData(ROOTFile,dataHistTemp,signalHistTemp,BR,tmin,tmax)
            signalAvg += signalTemp
          signalAvg *= 1.0 / len(signalPoint)
        
          tBins = [tmin,0.2,0.4,0.6,0.8,1.0,tmax]
          zBins = [0,2000]
          ntIter = nT
          nzIter = nZ
        
          #print "# of t bins = " + str(ntMax) + "\t # of z bins = " + str(nzMax)
        
          [tBins,zBins,maxSig] = optimize(dataTemp,signalAvg,tBins,zBins,ntIter,nzIter)
        
          sigMat[i,nT,nZ,j] = maxSig
          j += 1
        i += 1
        j = 0
    
      #sigMatByCat[i,:] = np.sqrt(sigMatByCat[0,:]**2 + sigMatByCat[1,:]**2)
      #if (np.any(sigMatByCat == -1)):
      #  sigMatByCat[i,np.argmin(np.sum(sigMatByCat == -1,0) < 1):] = -1
      #print str(nT + 1) + '\t' + str(nZ + 1) + '\t',
      #print str(eCuts[np.argmax(sigMatByCat,1)[2]]) + '\t' + str(np.amax(sigMatByCat,1)[2])
      
  print
  opt = np.get_printoptions()
  np.set_printoptions(threshold=np.inf)
  print sigMat
  np.set_printoptions(**opt)

  sigMat = np.where(sigMat == -1,0,sigMat)

  sigTot = np.sqrt(np.amax(np.amax(sigMat,1)[0],0)**2 + np.amax(np.amax(sigMat,1)[1],0)**2)
  print 'Sig by eCut: ' + str(sigTot)
  print 'Max Sig: ' + str(np.amax(sigTot)) + ' Energy: ' + str(eCuts[np.argmax(sigTot)])
  print 'g: [#t,#z] = ' + str(np.vstack([np.argmax(sigMat,1)[0][np.argmax(np.amax(sigMat,1)[0],0),range(0,len(eCuts))],np.argmax(np.amax(sigMat,1)[0],0)])[:,np.argmax(sigTot)] + [1,1])
  print 'gg: [#t,#z] = ' + str(np.vstack([np.argmax(sigMat,1)[1][np.argmax(np.amax(sigMat,1)[1],0),range(0,len(eCuts))],np.argmax(np.amax(sigMat,1)[1],0)])[:,np.argmax(sigTot)] + [1,1])

 
      #'''
    
  # Examine E cut
  '''
  eCuts = ['2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20']
  #eCuts = ['2','5','10']
  #signalList = ['60_55_2','60_50_2']
  #signalList = ['60_55_2','60_50_2','60_0_2','30_25_2','30_20_2','30_0_2']
  signalList = ['30_25_2','40_35_10','40_35_2','50_45_2','30_25_10','30_20_2','50_45_10','30_10_2','60_55_2','30_0_2','40_30_2','60_55_10','50_40_2','30_20_10','40_20_2','40_10_2','40_0_2','60_50_2','40_30_10','50_30_2','50_20_2','30_10_10','50_0_2','30_0_10','50_10_2','60_40_2','60_30_2','50_40_10','60_10_2','60_20_2','60_0_2','40_20_10','40_10_10','40_0_10','60_50_10','50_30_10','50_20_10','50_10_10','50_0_10','60_40_10','60_30_10','60_20_10','60_10_10','60_0_10']
  #catList = ['1lp_1ph','1lp_2ph','2lp_1ph','2lp_2ph']
  catList = ['nlp_1ph','nlp_2ph']

  for s in signalList:
    #print '******************************'
    #print 'Signal: ' + str(s)
    #print '******************************'
    sigMatByCat = np.zeros((len(catList) + 1,len(eCuts)))
    i = 0
    j = 0
    for c in catList:
      #print '********************'
      #print 'Channel: ' + str(c)
      #print '********************'
      for en in eCuts:
        #print '**********'
        #print 'E cut: ' + str(en) + ' GeV'
        #print '**********'

        [data, signal] = formatData(ROOTFile,'TIMING_POINTING_{1}GeV__SR_{0}_data'.format(c,en),'TIMING_POINTING_{2}GeV__SR_{0}_{1}'.format(c,s,en),BR,tmin,tmax)

        # FIXME Fixing number of bins
        tBins = [tmin,0.2,0.4,0.6,0.8,1.0,tmax]
        zBins = [0,2000]

        ntIter = ntMax - 1
        nzIter = nzMax - 1

        [tBins,zBins,maxSig] = optimize(data,signal,tBins,zBins,ntIter,nzIter)

        #print str(maxSig) + '\t',

        # FIXME Fixing nunmber of bins
        #sigMat = np.zeros((ntMax,nzMax))
        #tBinsMat = np.zeros((ntMax,nzMax),dtype=object)
        #zBinsMat = np.zeros((ntMax,nzMax),dtype=object) 
        #for ntIter in range(0,ntMax):
        #  for nzIter in range(0,nzMax):
      
        #    tBins = [tmin,0.2,0.4,0.6,0.8,1.0,tmax]
        #    zBins = [0,2000]

        #    ntIterNew = ntIter
        #    nzIterNew = nzIter
      
        #    if (ntIter != 0 or nzIter != 0):
        #      diag = (ntIter - 1) if (ntIter == nzIter) else min(ntIter,nzIter)
        #      tBins = tBinsMat[diag,diag]
        #      zBins = zBinsMat[diag,diag]
        #      ntIterNew = ntIter - diag
        #      nzIterNew = nzIter - diag
      
        #    [tBins,zBins,maxSig] = optimize(data,signal,tBins,zBins,ntIterNew,nzIterNew)
      
        #    #sigMat[ntIter,nzIter] = maxSig
        #    #tBinsMat[ntIter,nzIter] = tBins
        #    #zBinsMat[ntIter,nzIter] = zBins   
       
        ## Print best bins
        ##bestIndices = np.where(sigMat == np.amax(sigMat))
        ##print "-----Optimal Binning-----"
        ##print "# of t bins = " + str(bestIndices[0] + 1) + "\t # of z bins = " + str(bestIndices[1] + 1)
        ##print "Time Binning:"
        ##print tBinsMat[bestIndices[0],bestIndices[1]]
        ##print "Pointing Binning: "
        ##print zBinsMat[bestIndices[0],bestIndices[1]]
        ##print "Significance: " + str(sigMat[bestIndices[0],bestIndices[1]])
        ##print ' '.join(map(str,sigMat[bestIndices[0],bestIndices[1]])) + '\t',
        #print ' '.join(map(str,sigMat[bestIndices[0],bestIndices[1]])) + '\t',
        ##if (sigMat[int(bestIndices[0]),int(bestIndices[1])] > optSig):
        ##  optSig = sigMat[int(bestIndices[0]),int(bestIndices[1])]
        ##  optE   = en
        #sigMatByCat[i,j] = sigMat[int(bestIndices[0]),int(bestIndices[1])]
        sigMatByCat[i,j] = maxSig
        j += 1
      #print str(optE) + '\t' + str(optSig) + '\t',
      i += 1
      j = 0
    #sigMatByCat[i,:] = np.sqrt(sigMatByCat[0,:]**2 + sigMatByCat[1,:]**2 + sigMatByCat[2,:]**2 + sigMatByCat[3,:]**2)
    # FIXME Changed to only 2 channels
    sigMatByCat[i,:] = np.sqrt(sigMatByCat[0,:]**2 + sigMatByCat[1,:]**2)
    if (np.any(sigMatByCat == -1)):
      sigMatByCat[i,np.argmin(np.sum(sigMatByCat == -1,0) < 1):] = -1
    print s + '\t',
    print str(eCuts[np.argmax(sigMatByCat,1)[0]]) + '\t' + str(np.amax(sigMatByCat,1)[0]),
    print str(eCuts[np.argmax(sigMatByCat,1)[1]]) + '\t' + str(np.amax(sigMatByCat,1)[1]),
    print str(eCuts[np.argmax(sigMatByCat,1)[2]]) + '\t' + str(np.amax(sigMatByCat,1)[2]),
    # FIXME
    #print str(eCuts[np.argmax(sigMatByCat,1)[3]]) + '\t' + str(np.amax(sigMatByCat,1)[3]),
    #print str(eCuts[np.argmax(sigMatByCat,1)[4]]) + '\t' + str(np.amax(sigMatByCat,1)[4]),
    print
  '''
  
  # Examine varying tmax
  '''
  tmaxVals = np.arange(4,18,1)
  sigVals = []

  for tTest in tmaxVals:

    print 'tmax = {0}'.format(tTest)
    [data,signal] = formatData(ROOTFile,dataHist,signalHist,BR,tmin,tTest)
    tBins = [tmin,tTest]
    zBins = [0,2000]
    [tBins,zBins,maxSig] = optimize(data,signal,tBins,zBins,4,1)
    print "Time Binning:"
    print tBins
    print "Pointing Binning: "
    print zBins
    print "Significance: " + str(maxSig) + "\n"
    sigVals.append(maxSig)

  plt.plot(tmaxVals,sigVals,'ko')
  #plt.show()
  plt.xlim(tmaxVals[0] - 1,tmaxVals[-1] + 1)
  plt.xlabel('tmax [ns]')
  plt.ylabel('Expected Significance')
  plt.savefig('sigVstmax.png')  
  #'''

  # Optimization
  '''
  sigMat = np.zeros((ntMax,nzMax))
  tBinsMat = np.zeros((ntMax,nzMax),dtype=object)
  zBinsMat = np.zeros((ntMax,nzMax),dtype=object) 
  for ntIter in range(0,ntMax):
    for nzIter in range(0,nzMax):

      #print "# of t bins = " + str(ntIter + 1) + "\t # of z bins = " + str(nzIter + 1)

      tBins = [tmin,0.2,0.4,0.6,0.8,1.0,tmax]
      zBins = [0,2000]
      ntIterNew = ntIter
      nzIterNew = nzIter

      # Separate 1D optimizations
      #[tBins,zBins,maxSig] = optimize(data,signal,tBins,zBins,ntIterNew,0)
      #tBins = [tmin,tmax]
      #[tBins,zBins,maxSig] = optimize(data,signal,tBins,zBins,0,nzIterNew)
      #tBins = [tmin,0.2,0.4,0.6,0.8,1.0,tmax]
      #[tBins,zBins,maxSig] = optimize(data,signal,tBins,zBins,ntIterNew,0)

      # Simultaneous optimization
      if (ntIter != 0 or nzIter != 0):
        diag = (ntIter - 1) if (ntIter == nzIter) else min(ntIter,nzIter)
        tBins = tBinsMat[diag,diag]
        zBins = zBinsMat[diag,diag]
        ntIterNew = ntIter - diag
        nzIterNew = nzIter - diag
 
      [tBins,zBins,maxSig] = optimize(data,signal,tBins,zBins,ntIterNew,nzIterNew)

      sigMat[ntIter,nzIter] = maxSig
      tBinsMat[ntIter,nzIter] = tBins
      zBinsMat[ntIter,nzIter] = zBins   
 
      #print "Time Binning:"
      #print tBins
      #print "Pointing Binning: "
      #print zBins
      #print "Significance: " + str(maxSig) + "\n"

      #savePlots(data,signal,tBins,zBins)

  # Print best bins
  bestIndices = np.where(sigMat == np.amax(sigMat))
  print "-----Optimal Binning-----"
  print "# of t bins = " + str(bestIndices[0] + 1) + "\t # of z bins = " + str(bestIndices[1] + 1)
  print "Time Binning:"
  print tBinsMat[bestIndices[0],bestIndices[1]]
  print "Pointing Binning: "
  print zBinsMat[bestIndices[0],bestIndices[1]]
  print "Significance: " + str(sigMat[bestIndices[0],bestIndices[1]])

  #sigHist = TH2F('sigHist','Cat {0} Expected Significance;# of t Bins;# of z Bins'.format(cat),ntMax,0.5,ntMax + 0.5,nzMax,0.5,nzMax + 0.5)
  #array2hist(sigMat,sigHist)
  #sigHist.Draw('textcolz')
  #gStyle.SetOptStat(0)
  #gStyle.SetPaintTextFormat('4.3f')
  #c1.SetLogy(0)
  #c1.SaveAs('results/plots/sigMatrix_{0}_t{1}_z{1}.png'.format(signalHist,ntMax,nzMax))

  ## Save results
  #fSave = TFile('results/sigMatrix/results_{0}_t{1}_z{1}.root'.format(signalHist,ntMax,nzMax),'RECREATE')
  #sigHist.Write()
  #fSave.Close()
  ##sys.stdout = textOutput
  ##fText.close()

  ## Find minimum number of bins with at least 90% of the max. binning
  #minRow = ntMax
  #minCol = nzMax
  #thresh = 0.99
  #for i in range(sigMat.shape[0]):
  #  for j in range(sigMat.shape[1]):
  #    #print str(i) + " " + str(j) + " " + str(sigMat[i][j]) + " " + str(minRow) + " " + str(minCol) + " " + str(np.max(sigMat))
  #    if sigMat[i][j] >= thresh * np.max(sigMat) and ((i + 1) * (j + 1)) < (minRow * minCol):
  #      minRow = i + 1
  #      minCol = j + 1

  #print str(thresh * 100) + "% max bins with minimum binning: " + "# of t bins = " + str(minRow) + "\t # of z bins = " + str(minCol)
  #print "Time Binning:"
  #print tBinsMat[minRow - 1,minCol - 1]
  #print "Pointing Binning: "
  #print zBinsMat[minRow - 1,minCol - 1]
  #print "Significance: " + str(sigMat[minRow - 1,minCol - 1])

  #minRow = ntMax
  #minCol = nzMax
  #thresh = 0.95
  #for i in range(sigMat.shape[0]):
  #  for j in range(sigMat.shape[1]):
  #    #print str(i) + " " + str(j) + " " + str(sigMat[i][j]) + " " + str(minRow) + " " + str(minCol) + " " + str(np.max(sigMat))
  #    if sigMat[i][j] >= thresh * np.max(sigMat) and ((i + 1) * (j + 1)) < (minRow * minCol):
  #      minRow = i + 1
  #      minCol = j + 1

  #print str(thresh * 100) + "% max bins with minimum binning: " + "# of t bins = " + str(minRow) + "\t # of z bins = " + str(minCol)
  #print "Time Binning:"
  #print tBinsMat[minRow - 1,minCol - 1]
  #print "Pointing Binning: "
  #print zBinsMat[minRow - 1,minCol - 1]
  #print "Significance: " + str(sigMat[minRow - 1,minCol - 1])
  #'''

  endTime = time.time()

  print "Finished in {0:.1f} s".format(endTime - startTime)

  return

if __name__ == '__main__':

    main()
